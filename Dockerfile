FROM tomcat:9-jdk8
LABEL maintainer="Black Cow Technology <tech@blackcowtech.uk>"

ENV \
	CATALINA_HOME= \
	APP_UID=9000 \
	APP_GID=9000 \
	APP_USER=atlassian \
	APP_GROUP=atlassian \
	APP_HOME=/var/atlassian/application-data \
	APP_INSTALL=/opt/atlassian

RUN set -ex \
	&& addgroup --gid $APP_GID $APP_GROUP \
	&& adduser --uid $APP_UID --gid $APP_GID \
	--disabled-password \
	--shell /bin/bash \
	--gecos '' \
	$APP_USER \
	&& echo "JAVA_HOME=$JAVA_HOME" >> /etc/profile

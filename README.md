# Atlassian Base Docker Image

Base Docker image for BCT Atlassian applications:

* [blackcowtech/atlassian-bamboo](https://gitlab.com/blackcowtech/atlassian/bamboo)
* [blackcowtech/atlassian-bitbucket](https://gitlab.com/blackcowtech/atlassian/bitbucket)

Much of the inspiration for these Dockerfiles has come from [blacklabelops](https://github.com/blacklabelops) and [cptactionhank](https://github.com/cptactionhank).
